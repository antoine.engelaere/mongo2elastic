package com.sandbox.mongo2elastic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mongo2elasticApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mongo2elasticApplication.class, args);
	}

}
