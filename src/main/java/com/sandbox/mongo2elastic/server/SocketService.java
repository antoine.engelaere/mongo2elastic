package com.sandbox.mongo2elastic.server;

import com.mongodb.reactivestreams.client.MongoClient;
import com.sandbox.mongo2elastic.JsonUtils;
import org.reactivestreams.Subscription;
import org.springframework.data.mongodb.core.ChangeStreamOptions;
import org.springframework.data.mongodb.core.ChangeStreamOptions.ChangeStreamOptionsBuilder;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;

import java.util.Arrays;

@Component
public class SocketService implements WebSocketHandler {

    private final ReactiveMongoTemplate reactiveMongoTemplate;
    private final MongoClient mongoClient;

    private final ThreadLocal<Subscription> subscription = new ThreadLocal<>();

    public SocketService(ReactiveMongoTemplate reactiveMongoTemplate, MongoClient mongoClient) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
        this.mongoClient = mongoClient;
    }

    @Override
    public Mono<Void> handle(WebSocketSession webSocketSession) {
        return webSocketSession
                .send(
                        webSocketSession.receive()
                                .switchMap(message ->
                                        reactiveMongoTemplate
                                                .changeStream(
                                                        optBuilder(message)
                                                                .returnFullDocumentOnUpdate()
                                                                .build(),
                                                        Object.class
                                                )
                                                .doOnNext(System.out::println)
                                                .map(ChangeStreamEventMapper::toDTO)
                                                .map(JsonUtils::toString)
                                                .map(webSocketSession::textMessage)
                                                .doOnSubscribe(newSubscription -> {
                                                    Subscription old = subscription.get();
                                                    subscription.set(newSubscription);
                                                    if (old != null) {
                                                        old.cancel();
                                                    }
                                                })
                                                .log("changeStream")
                                )
                                .log("receive")
                ).log("send");
    }

    private static ChangeStreamOptionsBuilder optBuilder(WebSocketMessage message) {
        ChangeStreamOptionsBuilder optBuilder = ChangeStreamOptions.builder();
        String payload = message.getPayloadAsText();
        if (!payload.isEmpty()) {
            Criteria[] fieldsExistsCriteria = Arrays.stream(payload.split(","))
                    .map(field -> Criteria.where(field).exists(true))
                    .toArray(Criteria[]::new);
            Aggregation agg = Aggregation.newAggregation(
                    Aggregation.match(
                            new Criteria().orOperator(
                                    fieldsExistsCriteria
                            )
                    )
            );
            optBuilder.filter(agg);
        }
        return optBuilder;
    }
}