package com.sandbox.mongo2elastic.server;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.OperationType;
import com.sandbox.mongo2elastic.JsonUtils;
import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.data.mongodb.core.ChangeStreamEvent;

import java.time.Instant;
import java.util.Optional;

public class ChangeStreamEventMapper {

    public static final ChangeDTO toDTO(ChangeStreamEvent<Object> streamEvent) {
//        streamEvent.getResumeToken()
        ChangeDTO dto = new ChangeDTO();
        Optional.ofNullable(streamEvent.getOperationType())
                .map(OperationType::getValue)
                .ifPresent(dto::setType);
        Optional.ofNullable(streamEvent.getTimestamp())
                .map(Instant::toEpochMilli)
                .ifPresent(dto::setTimestamp);
        Optional.ofNullable(streamEvent.getRaw())
                .map(ChangeStreamDocument::getDocumentKey)
                .map(BsonDocument::toJson)
                .map(JsonUtils::readJson)
                .flatMap(JsonUtils::getId)
                .ifPresent(dto::setId);
        Optional.ofNullable(streamEvent.getRaw())
                .map(ChangeStreamDocument::getFullDocument)
                .map(Document::toJson)
                .map(JsonUtils::readJson)
                .map(JsonUtils::removeId)
                .ifPresent(dto::setValue);
        return dto;
    }

}
