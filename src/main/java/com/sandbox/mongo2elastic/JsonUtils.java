package com.sandbox.mongo2elastic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Optional;

public class JsonUtils {
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static final <T> T readJson(final String value, final Class<T> target) {
        try {
            return OBJECT_MAPPER.readerFor(target).readValue(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static final JsonNode readJson(final String value) {
        try {
            return OBJECT_MAPPER.readTree(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return NullNode.getInstance();
        }
    }

    public static final String toString(final Object o) {
        try {
            return OBJECT_MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final JsonNode toJson(final Object o) {
        return readJson(toString(o).toString());
    }

    public static final Optional<String> getId(final JsonNode node) {
        return Optional.ofNullable(node)
                .map(json -> json.get("_id"))
                .map(JsonNode::asText);
    }

    public static final JsonNode removeId(final JsonNode node) {
        if (node instanceof ObjectNode) {
            ((ObjectNode) node).remove("_id");
        }
        return node;
    }
}
