package com.sandbox.mongo2elastic.client;

import com.sandbox.mongo2elastic.JsonUtils;
import com.sandbox.mongo2elastic.server.ChangeDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Objects;

public class ReactiveJavaClientWebSocket {
    public static void main(String[] args) throws InterruptedException {
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("kbn-version", "6.0.1");
        UriComponentsBuilder uribuilder = UriComponentsBuilder
                .fromUri(URI.create("http://localhost:19200"))
                .path("/test")
                .path("/_doc");


        WebSocketClient client = new ReactorNettyWebSocketClient();
        client.execute(
                URI.create("ws://localhost:8080/watch"),
                session -> session.send(
                        Mono.just(session.textMessage("")))
                        .thenMany(
                                session.receive()
                                        .map(WebSocketMessage::getPayloadAsText)
                                        .doOnNext(payload -> {
                                            ChangeDTO dto = JsonUtils.readJson(payload, ChangeDTO.class);
                                            String type = Objects.toString(dto.getType(), "");
                                            switch (type) {
                                                case "delete":
                                                    rest.delete(
                                                            uribuilder.cloneBuilder().path("/" + dto.getId()).build().toUri()
                                                    );
                                                    break;
                                                case "insert":
                                                case "replace":
                                                    rest.postForEntity(
                                                            uribuilder.cloneBuilder().path("/" + dto.getId()).build().toUri(),
                                                            new HttpEntity<>(dto.getValue(), headers),
                                                            String.class
                                                    );
                                                    break;
                                                default:
                                                    System.err.println("Not supprted " + type);
                                            }
                                        })
                                        .log()
                        )
                        .then()
        )
                .block();
    }

}