#!/bin/bash

echo 'PS : IM not a bash expert !!!'

docker stop mongo-node1 mongo-node2 mongo-node3 mongo-node4 mongo-node5 mongo-node6 mongo-config2 mongo-server
docker rm  mongo-node1 mongo-node2 mongo-node3 mongo-node4 mongo-node5 mongo-node6 mongo-config2 mongo-server

docker run --name mongo-node1 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs0" --shardsvr --port 27017
docker run --name mongo-node2 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs0" --shardsvr --port 27017
docker run --name mongo-node3 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs0" --shardsvr --port 27017

docker exec -it mongo-node1 bash -c "echo 'rs.initiate({_id : \"rs0\", members: [{ _id : 0, host : \"mongo-node1:27017\" },{ _id : 1, host : \"mongo-node2:27017\" },{ _id : 2, host : \"mongo-node3:27017\" }]})' | mongo 127.0.0.1:27017"

docker run --name mongo-node4 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs1" --shardsvr --port 27020
docker run --name mongo-node5 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs1" --shardsvr --port 27020
docker run --name mongo-node6 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs1" --shardsvr --port 27020
docker run --name mongo-node7 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs1" --port 30000
docker exec -it mongo-node4 bash -c "echo 'rs.initiate({_id : \"rs1\", members: [{ _id : 0, host : \"mongo-node4:27020\" },{ _id : 1, host : \"mongo-node5:27020\" },{ _id : 2, host : \"mongo-node6:27020\" }]})' | mongo 127.0.0.1:27020"

docker run --name mongo-config2 -d --net my-mongo-cluster  mongo:3.4.6 --configsvr --replSet mongoConfig --port 27019

docker exec -it mongo-config2 bash -c "echo 'rs.initiate({_id: \"mongoConfig\", configsvr: true, members: [{ _id : 0, host : \"mongo-config2:27019\" }]})' | mongo 127.0.0.1:27019"

docker run --name mongo-server -p 27017:27017 --expose 27017 -d --net my-mongo-cluster mongo:3.4.6 mongos --configdb mongoConfig/mongo-config2:27019 --port 27017
sh.addShard( "rs0/mongo-node1:27017,mongo-node2:27017,mongo-node3:27017" );
sh.addShard( "rs1/mongo-node4:27020,mongo-node5:27020,mongo-node6:27020" );

docker run --name mongo-node8 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs2" --shardsvr --port 27021
docker run --name mongo-node9 -d --net my-mongo-cluster  mongo:3.4.6 --replSet "rs2" --shardsvr --port 27021
docker exec -it mongo-node8 bash -c "echo 'rs.initiate({_id : \"rs1\", members: [{ _id : 0, host : \"mongo-node8:27021\" },{ _id : 1, host : \"mongo-node9:27021\" }]})' | mongo 127.0.0.1:27021"

docker start mongo-node1 mongo-node2 mongo-node3 mongo-node4 mongo-node5 mongo-node6 mongo-config2 mongo-server


var cfg = rs.conf(); cfg.members[4].priority = 1;cfg.members[4].votes = 1;rs.reconfig(cfg)