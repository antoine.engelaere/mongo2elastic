"C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --replSet rs0 --port 27030 --bind_ip_all --dbpath C:\workspace\sandbox\mongo2elastic\db\primary --oplogSize 128
"C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --replSet rs0 --port 27031 --bind_ip_all --dbpath C:\workspace\sandbox\mongo2elastic\db\secondary --oplogSize 128

"C:\Program Files\MongoDB\Server\4.2\bin\mongo.exe" --port 27017

rsconf = {
  _id: "rs0",
  members: [
    {
     _id: 0,
     host: "localhost:27030"
    },
    {
     _id: 1,
     host: "localhost:27031"
    }
   ]
}

rs.initiate( rsconf )


use batica
db.createCollection('company')
show collections

db.company.remove({"_id": "test"})
db.company.insert({"_id": "test", "test1": "insert"})
db.company.update({"_id": "test"}, {"test1": "update"})
db.company.remove({"_id": "test"})
